let userName = prompt ('Enter your name');
let userLastName = prompt ('Enter your last name');

let student = {     
    name: userName,
    lastName : userLastName,
    table: {}
};

while(true){
let subject = prompt ('Enter the name of the subject');
if (subject === null){
    break;
}
  
let grade = prompt (`Enter the grade for the subject '${subject}':`);
student.table[subject] = grade;
if (grade < 4){
    console.log(`The number of bad grades: ${grade.length}`)
}else{
    console.log('The student is transferred to the next course');
}
}



console.log('Student Information:');
console.log(`Name: ${student.name}`);
console.log(`Last Name: ${student.lastName}`);
console.log('Grades:');

for (let subject in student.table) {
    let grade = student.table[subject];
    console.log (`Subject: ${subject} ${grade}`);
}